package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Task;

import java.util.Date;
import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task create(String name, String description, Date dateBegin, Date dateEnd);

    List<Task> findAllByProjectId(String projectId);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    void changeTaskStatusById(String id, Status status);

    void changeTaskStatusByIndex(Integer index, Status status);

}

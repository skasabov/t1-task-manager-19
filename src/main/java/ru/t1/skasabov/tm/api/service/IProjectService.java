package ru.t1.skasabov.tm.api.service;

import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Date;

public interface IProjectService extends IService<Project> {

    Project create(String name);

    Project create(String name, String description);

    Project create(String name, String description, Date dateBegin, Date dateEnd);

    void updateById(String id, String name, String description);

    void updateByIndex(Integer index, String name, String description);

    void changeProjectStatusById(String id, Status status);

    void changeProjectStatusByIndex(Integer index, Status status);

}
